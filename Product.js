const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
const Schema = mongoose.Schema;

mongoose.plugin(slug);

const Product = new Schema({
    name: { type: String, required: true },
    description: { type: String },
    image: { type: String },
    paragraph1: { type: String },
    paragraph2: { type: String },
    paragraph3: { type: String },
    paragraph4: { type: String },
    productImage1: { type: String },
    productImage2: { type: String },
    productImage3: { type: String },
    productImage4: { type: String },
    productImage5: { type: String },
    productImage6: { type: String },
    slug: { type: String, slug: 'name', unique: true },
}, {
    timestamps: true,
});

module.exports = mongoose.model('Product', Product);