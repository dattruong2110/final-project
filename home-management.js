const express = require('express');
const router = express.Router();

const HomeManageController = require('../app/controllers/home-manage-controller');

router.use('/home-management', HomeManageController.index);

module.exports = router;