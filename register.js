const express = require('express');
const router = express.Router();

const RegisterController = require('../app/controllers/register-controller');

router.use('/register', RegisterController.index);

module.exports = router;