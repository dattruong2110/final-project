const { default: mongoose } = require('mongoose');
const Product = require('../models/Product');
const Banner = require('../models/Banner');
const { multipleMongooseToObject } = require('../../util/mongoose');

class HomeManageController {
    // [GET] /login
    index(req, res, next) {
        Product.find({})
            .then(products => {
                res.render('home-management', { 
                    products: multipleMongooseToObject(products)
                });
            })
            .catch(next);
    }
}

module.exports = new HomeManageController();