class ProductDetailsController {
    // [GET] /product-details
    index(req, res) {
        res.render('product-details');
    }
}

module.exports = new ProductDetailsController;