const homeRouter = require('./home');
const productsRouter = require('./products');
const productDetailsRouter = require('./product-details');
const product2DetailsRouter = require('./product2-details');
const loginRouter = require('./login');
const registerRouter = require('./register');
const homeManageRouter = require('./home-management')

function route(app) {
    app.get('/', homeRouter);
    app.use('/products', productsRouter);
    app.get('/product-details', productDetailsRouter);
    app.get('/product2-details', product2DetailsRouter);
    app.get('/login', loginRouter);
    app.get('/register', registerRouter);
    app.get('/home-management', homeManageRouter);

    // app.get('/', (req, res) => {
    //     res.render('home');
    //   })
      
    //   app.get('/login-form', (req, res) => {
    //     res.render('login-form');
    //   })
      
    //   app.post('/login-form', (req, res) => {
    //     res.render('login-form');
    //   })
      
    //   app.get('/register-form', (req, res) => {
    //     res.render('register-form');
    //   })
      
    //   app.post('/register-form', (req, res) => {
    //     res.render('register-form');
    //   })
      
      // app.get('/product-details', (req, res) => {
      //   res.render('product-details');
      // })
      
    //   app.get('/product2-details', (req, res) => {
    //     // console.log(req.query.q) query parameters (syntax: http://localhost:3000/product2-details?q=abc&ref=xyz) use for search function
    //     // console.log(req.query.ref)
    //     res.render('product2-details');
    //   })
}

module.exports = route;