const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Banner = new Schema({
    banner_image: { type: String },
}, {
    timestamps: true,
});

module.exports = mongoose.model('Banner', Banner);