const express = require('express');
const router = express.Router();

const product2DetailsController = require('../app/controllers/product2-details-controller');

router.get('/product2-details', product2DetailsController.index);

module.exports = router;