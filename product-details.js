const express = require('express');
const router = express.Router();

const productDetailsController = require('../app/controllers/product-details-controller');

router.get('/product-details', productDetailsController.index);

module.exports = router;