const { default: mongoose } = require('mongoose');
const Product = require('../models/Product');
const Banner = require('../models/Banner');
const { multipleMongooseToObject } = require('../../util/mongoose');

class HomeController {
    // [GET] /
    index(req, res, next) {
        // Banner.find({})
        //     .then(banners => {
        //         res.render('home', {
        //             banners: multipleMongooseToObject(banners)
        //         });
        //     })
        //     .catch(next);

        Product.find({})
            .then(products => {
                res.render('home', { 
                    products: multipleMongooseToObject(products)
                });
            })
            .catch(next);
    }
}

module.exports = new HomeController();