const express = require('express');
const router = express.Router();

const LoginController = require('../app/controllers/login-controller');

router.use('/login', LoginController.index);

module.exports = router;